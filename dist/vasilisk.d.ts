/**
 * Validation - method to be invoked to check the input value
 */
interface Validation {
    (elem: HTMLInputElement): boolean;
}
/**
 * onValid / onInvalid interface that brings back the group id
 */
interface Callback {
    (gid: string): void;
}
interface CallbackFailed {
    (gid: string, failedElements: any): void;
}
interface DefaultOptions {
    options: {
        callBackOnEveryValid: boolean;
        chainedGroups: {
            groups: Array<string>;
            watchChainBroken: boolean;
            brokenChainCallback: Function;
        };
    };
}
declare type Group = {
    gid: string;
    elems: Array<{
        e: {
            internal: HTMLInputElement;
            isValid: boolean;
            failed: Function;
        };
        validation: Validation;
    }>;
    onValid: Callback;
    onInvalid: CallbackFailed;
    isValid: boolean;
    isDirty: boolean;
};
export declare class Vasilisk implements DefaultOptions {
    /**
     * options - vasilisk initialiser options
     */
    options: {
        callBackOnEveryValid: boolean;
        chainedGroups: {
            groups: Array<string>;
            watchChainBroken: boolean;
            brokenChainCallback: Function;
        };
    };
    /**
     * groups - property that holds final array of validation groups
     */
    groups: Array<Group>;
    /**
     * useRanking - property that identifies whether ranking is used
     */
    useRanking: boolean;
    /**
     * constructor - Vasilisk constructor
     * @param options Object - Vasilisk options
     *
     * @returns Vasilisk
     */
    constructor(options?: {
        callBackOnEveryValid: boolean;
        chainOrder?: Array<string>;
        watchBroken?: boolean;
        brokenCallback?: Function;
    });
    /**
     * CreateGroup - create a logical group of elements
     *
     * @param gid string - group id
     * @param onValid function - callback triggered when group is valid
     * @param onInvalid function - callback triggered when group is invalid
     * @param elems array - input fields to be watched
     *
     * @returns void
     */
    CreateGroup(gid: string, onValid: Callback, onInvalid: Callback, elems: Array<{
        id: string;
        validation: Validation;
        pristine?: boolean;
        failed?: Function;
    }>): void;
    /**
     * DevNull - > /dev/null :)
     */
    private DevNull;
    /**
     * IsCurrentlyValid - poll if the specified group is in a valid state.
     *
     * @param gid string - group id
     *
     * @returns Promise<boolean>
     */
    IsCurrentlyValid(gid: string): Promise<boolean>;
    /**
     * ForceValidationQuiet - Re-evaluate group validity
     *
     * @param gid string - Group id
     * @param isDirty boolean - mark group as Dirty
     */
    ForceValidationQuiet(gid: string, isDirty?: boolean): Promise<boolean>;
    /**
    * ForceValidation - immitate change event and call specified callbacks.
    * ! Marks a group as dirty
    *
    * @param gid string - Group id
    * * @param isDirty boolean - mark group as Dirty
    */
    ForceValidation(gid: string, isDirty?: boolean): void;
    /**
     * ForceValidationQuietChained - Reevaluate all Vasilisk groups and mark top group as dirty
     *
     * @param ignoreGroupsIds Array<string> - groups to ignore in validation
     * @default []
     *
     * @returns Promise string | null (if non of groups is valid)
     */
    ForceValidationQuietChained(ignoreGroupsIds?: Array<string>): Promise<string | null>;
    /**
     * BindEventListener - apply event listeners on elements in the group
     *
     * @returns void
     */
    private BindEventListener;
    /**
     * Watch - polling group validity and invoking corresponding callbacks
     *
     * @returns void
     */
    private WatchDirty;
    /**
     * WatchDirty - polling group validity + change if occured in already valid fields and invoking corresponding callbacks
     *
     * @returns void
     */
    private Watch;
    /**
     * GetAllInvalidFields - get all invalid predecessors' inputs of the current group
     * @description Only if group ranking is assigned
     * @param gid string - group id
     *
     * @returns Array<{gid: string, elem: HTMLInputElement}>
     */
    private GetAllInvalidFields;
    /**
     * GetTopValidGroup - Get the top-most valid group
     * @description Only if group ranking is assigned
     *
     * @param gid string - group id
     *
     * @returns Promise - group if found, undefined otherwise
     */
    private GetTopValidGroup;
    /**
     * GetGroupRankingIndex - get current rank from ranking array
     * @description Only if group ranking is assigned
     *
     * @param gid string - group id
     *
     * @returns number - index in ranking
     */
    private GetGroupRankingIndex;
    /**
     * GetGroupById - Get the group instance by its group id
     *
     * @param gid string - group id
     *
     * @returns Promise - group if found, undefined otherwise
     */
    private GetGroupById;
    /**
     * GetDOMElement - Retrieve input on document
     * @param id string - <input id='{{this}}'>
     * @returns HTMLInputElement
     */
    private GetDOMElement;
    /**
     * GroupExists - checks if a group exists in a context
     * @param gid string - group id
     *
     * @returns boolean
     */
    private GroupExists;
}
export {};
