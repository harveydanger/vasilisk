# Changes to the Vasilisk project

### v1.0.22
- ForceValidationQuietChain - added ignore ids parameter to skip specified groups

### v1.0.21
- Added function ForceValidationQuietChain - validates the group without invoking a callback, marking the top most valid group in chain as dirty. This makes it easier to call validity callback on chains
- ForceValidation and ForceValidationQuiet - added default `isDirty: boolean = true` parameter for ForceValidationQuietChain support

### v1.0.19
- Added function ForceValidationQuiet - validates the group without invoking a callback

### v1.0.17
- Various bug fixing

### v1.0.15
- Added ForceValidation - function to invoke validation of a group

### v1.0.11
- Added an optional callback on validation failed for each input in a group

### v1.0.10
- New option - group chaining. See README for details
- Various bug fixes
- Extended example.html

### v1.0.9
- Promise wrapping for a quick test functions

### v1.0.8
- Added feature to get current group validity on a onValid callback

### v1.0.7 
- Added the ability to check the validity of the input group with an Event, rather than polling
- Added feature to support pristine inputs - by default and with preset values inputs are valid

### v1.0.6 
- Calculations are based on polling