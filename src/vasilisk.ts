/*
    Vasilisk - input field validator and event trigger
    Copyright (C) 2019 Vladimir Sukhov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
 */


declare var document: Document;

/**
 * Validation - method to be invoked to check the input value
 */
interface Validation {
   (elem: HTMLInputElement) : boolean
}

/**
 * onValid / onInvalid interface that brings back the group id
 */
interface Callback {
    (gid: string) : void
}

interface CallbackFailed{
    (gid: string, failedElements: any) : void
}

interface DefaultOptions{
    options: {
        callBackOnEveryValid: boolean
        chainedGroups: {
            groups: Array<string>,
            watchChainBroken: boolean,
            brokenChainCallback: Function
        }
    }
}

type Group = {
    gid: string, elems: Array<
        {e: {internal: HTMLInputElement, isValid: boolean, failed: Function},
        validation: Validation
        }>, 
        onValid: Callback, 
        onInvalid: CallbackFailed, 
        isValid: boolean,
        isDirty: boolean
}

export class Vasilisk implements DefaultOptions{

    /**
     * options - vasilisk initialiser options
     */
    options: {
        callBackOnEveryValid: boolean,
        chainedGroups: {
            groups: Array<string>,
            watchChainBroken: boolean,
            brokenChainCallback: Function
        }
    };
    
    /**
     * groups - property that holds final array of validation groups
     */
    groups: Array<Group>;

    /**
     * useRanking - property that identifies whether ranking is used
     */
    useRanking: boolean;

    /**
     * constructor - Vasilisk constructor
     * @param options Object - Vasilisk options
     * 
     * @returns Vasilisk
     */
    public constructor(options?: {callBackOnEveryValid: boolean, chainOrder? : Array<string>, watchBroken?: boolean, brokenCallback?: Function}){
        this.groups = new Array<Group>()
        this.options = {
            callBackOnEveryValid: true,
            chainedGroups: {
                groups : new Array<string>(),
                watchChainBroken: false,
                brokenChainCallback: ()=>{}
            }
        }
        this.useRanking = false;

        if(options){
            this.options.callBackOnEveryValid = typeof options.callBackOnEveryValid === 'boolean' ? options.callBackOnEveryValid : true;
            
            if (options.chainOrder && options.chainOrder.length > 0){
                this.options.chainedGroups.groups = options.chainOrder;
                this.useRanking = true;
            }
            if(!this.useRanking && options.watchBroken){
                console.error("Vasilisk: cannot apply chain watching without a chain")
            }else if(this.useRanking && options.watchBroken){
                this.options.chainedGroups.watchChainBroken = options.watchBroken;
            }

            if(!this.useRanking && options.brokenCallback){
                console.error("Vasilisk: cannot assign chain broken watch callback without a chain")
            }else if(this.useRanking && options.brokenCallback){
                this.options.chainedGroups.brokenChainCallback = options.brokenCallback;
            }
            
        }
    }

    /**
     * CreateGroup - create a logical group of elements
     * 
     * @param gid string - group id
     * @param onValid function - callback triggered when group is valid
     * @param onInvalid function - callback triggered when group is invalid
     * @param elems array - input fields to be watched
     * 
     * @returns void
     */
    public CreateGroup(gid: string, onValid: Callback, onInvalid: Callback, elems: Array<{id: string, validation: Validation, pristine?: boolean, failed?:Function}>){
        this.GroupExists(gid).then(exists =>{
            if(!exists){
                const e = elems.map((value)=>{
                    const dome: HTMLInputElement = this.GetDOMElement(value.id) 
                    let pristine = value.pristine ? value.pristine : false;
                    let fcb = value.failed ? value.failed : this.DevNull;
                    return {e: {internal: dome, isValid: pristine, failed: fcb}, validation: value.validation};
                })
                this.groups.push({gid: gid, elems: e, onValid: onValid, onInvalid: onInvalid, isValid: false, isDirty: true});               
                this.BindEventListener(this.groups[this.groups.length-1]);
            }else{
                console.error("Group already exists " + exists.gid);
            }
        }).catch(e=>{
            console.error(e);
        })
    }

    /**
     * DevNull - > /dev/null :)
     */
    private DevNull(){}

    /**
     * IsCurrentlyValid - poll if the specified group is in a valid state.
     * 
     * @param gid string - group id
     * 
     * @returns Promise<boolean>
     */
    public IsCurrentlyValid(gid: string) : Promise<boolean>{
        return new Promise((resolve, reject) => {
            this.GroupExists(gid).then(group=>{
                if(group){
                    resolve(group.isValid);
                }else{
                    reject('group does not exist')
                }
            }).catch(e=>{
                console.error(e)
            })    
        });
        
        
    }

    /**
     * ForceValidationQuiet - Re-evaluate group validity
     * 
     * @param gid string - Group id
     * @param isDirty boolean - mark group as Dirty
     */
    public async ForceValidationQuiet(gid: string, isDirty: boolean = true) : Promise<boolean>{
        return new Promise((resolve, reject) => {
            this.GetGroupById(gid).then(g =>{
                if(g !== undefined){
                    const i = g.elems.length;
                    let overallValid: number = 0;
                    g.elems.forEach(e =>{
                        if(e.validation(e.e.internal) === true){
                            e.e.isValid = true;
                            overallValid+=1;
                            g.isDirty = isDirty;
                        }else{
                            e.e.isValid = false;
                        }
                    })
                    if(overallValid === g.elems.length){
                        g.isValid = true;
                    }
                        resolve(g.isValid)
                }else{
                    console.error("Couldn't find a group");
                    resolve(false);
                }
            }).catch(e=>{
                console.error(e)
                reject();
            }) 
        });
    }

     /**
     * ForceValidation - immitate change event and call specified callbacks. 
     * ! Marks a group as dirty
     * 
     * @param gid string - Group id
     * * @param isDirty boolean - mark group as Dirty
     */
    public ForceValidation(gid: string, isDirty: boolean = true){
        this.GetGroupById(gid).then(g =>{
            if(g !== undefined){
                const i = g.elems.length;
                g.elems.forEach(e =>{
                    if(e.validation(e.e.internal) === true){
                        e.e.isValid = true;
                        g.isDirty = isDirty;
                    }else{
                        e.e.isValid = false;
                        e.e.failed(e.e.internal);
                    }
                    if(this.options.callBackOnEveryValid){
                        this.WatchDirty();
                    }else{
                        this.Watch();   
                    }
                })
            }else{
                console.error("Couldn't find a group");
            }
        }).catch(e=>{
            console.error(e)
        })
    }

    /**
     * ForceValidationQuietChained - Reevaluate all Vasilisk groups and mark top group as dirty
     * 
     * @param ignoreGroupsIds Array<string> - groups to ignore in validation
     * @default []
     * 
     * @returns Promise string | null (if non of groups is valid)
     */
    public ForceValidationQuietChained(ignoreGroupsIds: Array<string> = []) : Promise<string | null>{
        if(!this.options.chainedGroups.groups || this.options.chainedGroups.groups.length === 0){
            console.error("Cannot use ForceValidationQuietChained function on a Vasilisk without a chain")
            return Promise.resolve(null);
        }
        return new Promise((resolve, reject) => {
            this.groups.forEach(async g =>{
                if(ignoreGroupsIds.length !== 0){
                    if(ignoreGroupsIds.indexOf(g.gid) !== -1){
                        return;
                    }else{
                        await this.ForceValidationQuiet(g.gid, false)    
                    }
                }else{
                    await this.ForceValidationQuiet(g.gid, false)
                }
            })
    
            this.GetTopValidGroup(this.groups[0].gid).then(group =>{
                if(group){
                    this.IsCurrentlyValid(group.gid).then(v =>{
                        if(v){
                            group.isDirty = true;
                            resolve(group.gid)
                        }else{
                            resolve(null);
                        }
                    })
                }else{
                    resolve(null)
                }
            })
        });
    }

    /**
     * BindEventListener - apply event listeners on elements in the group
     * 
     * @returns void
     */
    private BindEventListener(g: Group){
        const i = g.elems.length;
        g.elems.forEach(e =>{
            e.e.internal.addEventListener('change', (event)=>{
                if(e.validation(e.e.internal) === true){
                    e.e.isValid = true;
                    g.isDirty = true;
                }else{
                    e.e.isValid = false;
                    e.e.failed(e.e.internal);
                }
                if(this.options.callBackOnEveryValid){
                    this.WatchDirty();
                }else{
                    this.Watch();   
                }
            })
        })
    }

    /**
     * Watch - polling group validity and invoking corresponding callbacks
     * 
     * @returns void
     */
    private WatchDirty() {
        this.groups.forEach(g => {
            let numOfValidFields: number = 0;
            g.elems.forEach(e => {
                if (e.e.isValid) {
                    numOfValidFields++;
                }
            });
            if (numOfValidFields === g.elems.length && g.isDirty) {
                g.isValid = true;

                if (this.useRanking) {
                    this.GetTopValidGroup(g.gid).then(top => {
                        if (top) {
                            if (top.isValid) {
                                top.onValid(top.gid)
                            } else {
                                if (this.options.chainedGroups.watchChainBroken) {
                                    this.options.chainedGroups.brokenChainCallback(this.GetAllInvalidFields(top.gid))
                                } else {
                                    console.error('Vasilisk: Broken chain occured at: ' + top.gid)
                                }
                            }
                        } else {
                            console.error('Vasilisk: Broken chain occured')
                        }
                    }).catch(e => {
                        console.error(e);
                    })
                } else {
                    g.onValid(g.gid)
                }
                g.isDirty = false;
            } else if (numOfValidFields !== g.elems.length) {
                if (g.isValid) {
                    g.isValid = !g.isValid
                    g.onInvalid(g.gid, g.elems.filter((val, index, array) => {
                        return val.e.isValid === false;
                    }))
                }
            }
        })
    }

    /**
     * WatchDirty - polling group validity + change if occured in already valid fields and invoking corresponding callbacks
     * 
     * @returns void
     */
    private Watch() {
        this.groups.forEach(g => {
            let numOfValidFields: number = 0;
            g.elems.forEach(e => {
                if (e.e.isValid) {
                    numOfValidFields++;
                }
            })
            if (numOfValidFields === g.elems.length) {
                if (!g.isValid) {
                    g.isValid = !g.isValid
                    if (this.useRanking) {
                        this.GetTopValidGroup(g.gid).then(top => {
                            if (top) {
                                if (top.isValid) {
                                    top.onValid(top.gid)
                                } else {
                                    if (this.options.chainedGroups.watchChainBroken) {
                                        this.options.chainedGroups.brokenChainCallback(this.GetAllInvalidFields(top.gid))
                                    } else {
                                        console.error('Vasilisk: Broken chain occured at: ' + top.gid)
                                    }
                                }
                            } else {
                                console.error('Vasilisk: Broken chain occured')
                            }
                        }).catch(e => {
                            console.error(e);
                        })
                    } else {
                        g.onValid(g.gid)
                    }
                }
            } else {
                if (g.isValid) {
                    g.isValid = !g.isValid
                    g.onInvalid(g.gid, g.elems.filter((val, index, array) => {
                        return val.e.isValid === false;
                    }))
                }
            }
        })
    }

    /**
     * GetAllInvalidFields - get all invalid predecessors' inputs of the current group
     * @description Only if group ranking is assigned
     * @param gid string - group id
     * 
     * @returns Array<{gid: string, elem: HTMLInputElement}>
     */
    private async GetAllInvalidFields(gid: string): Promise<Array<{gid: string, elem: HTMLInputElement}>>{
        let curRank : number = this.GetGroupRankingIndex(gid);
        let failedItems = new Array<{gid: string, elem: HTMLInputElement}>();
        for(let back = curRank; back >= 0 ; back--){ // if chaining is broken
            let g: Group | undefined = await this.GetGroupById(this.options.chainedGroups.groups[back])
            if(g && !g.isValid){
                g.elems.forEach(e =>{
                    if(!e.e.isValid){
                        if(g){ // suppress LINTER warning only
                            failedItems.push({gid: g.gid, elem: e.e.internal})
                        }
                    }
                })
            }
        }
        return failedItems;
    }

    /**
     * GetTopValidGroup - Get the top-most valid group
     * @description Only if group ranking is assigned
     * 
     * @param gid string - group id
     * 
     * @returns Promise - group if found, undefined otherwise 
     */
    private async GetTopValidGroup(gid: string): Promise < Group | undefined > {
        let curRank: number = this.GetGroupRankingIndex(gid);
        let maxIdx: number = this.options.chainedGroups.groups.length

        if (this.options.chainedGroups.watchChainBroken) {
            for (let back = curRank; back >= 0; back--) { // if chaining is broken
                let g: any = await this.GetGroupById(this.options.chainedGroups.groups[back])
                if (g && !g.isValid) {
                    return g;
                }
            }
        }

        for (; curRank < maxIdx; curRank++) {
            let g: any = await this.GetGroupById(this.options.chainedGroups.groups[curRank]);
            if (g && g.isValid) {
                if (curRank + 1 < maxIdx) {
                    let next = await this.GetTopValidGroup(this.options.chainedGroups.groups[curRank + 1]);
                    if (next && next.isValid) {
                        return next;
                    } else {
                        return g;
                    }
                } else {
                    return g;
                }
            } else {
                return await this.GetGroupById(this.options.chainedGroups.groups[curRank - 1]);
            }
        }
    }

    /**
     * GetGroupRankingIndex - get current rank from ranking array
     * @description Only if group ranking is assigned
     * 
     * @param gid string - group id
     * 
     * @returns number - index in ranking
     */
    private GetGroupRankingIndex(gid: string) : number{
        return this.options.chainedGroups.groups.indexOf(gid);
    }

    /**
     * GetGroupById - Get the group instance by its group id
     * 
     * @param gid string - group id
     * 
     * @returns Promise - group if found, undefined otherwise
     */
    private GetGroupById(gid: string) : Promise<Group | undefined>{
        return new Promise((resolve, reject) => {
            this.GroupExists(gid).then(nextGroup =>{
                if(nextGroup){
                    resolve(nextGroup);
                }else{
                    resolve(undefined);
                }
            }).catch(e =>{
                reject(undefined);
            });    
        });
        
    }

    /**
     * GetDOMElement - Retrieve input on document
     * @param id string - <input id='{{this}}'>
     * @returns HTMLInputElement
     */
    private GetDOMElement(id: string): HTMLInputElement{
        const e : HTMLInputElement = <HTMLInputElement>document.getElementById(id);
        if(!e){
            console.error("Couldn\'t find " + id);
        }
        return e;
    }

    /**
     * GroupExists - checks if a group exists in a context
     * @param gid string - group id
     * 
     * @returns boolean
     */
    private GroupExists(gid: string): Promise<undefined | Group>{
        return new Promise((resolve, reject) => {
            this.groups.forEach(element => {
                if(element.gid === gid){
                    resolve(element);
                }
            });
            resolve(undefined);
        });
    }
}