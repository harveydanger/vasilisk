"use strict";
/*
    Vasilisk - input field validator and event trigger
    Copyright (C) 2019 Vladimir Sukhov

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var Vasilisk = /** @class */ (function () {
    /**
     * constructor - Vasilisk constructor
     * @param options Object - Vasilisk options
     *
     * @returns Vasilisk
     */
    function Vasilisk(options) {
        this.groups = new Array();
        this.options = {
            callBackOnEveryValid: true,
            chainedGroups: {
                groups: new Array(),
                watchChainBroken: false,
                brokenChainCallback: function () { }
            }
        };
        this.useRanking = false;
        if (options) {
            this.options.callBackOnEveryValid = typeof options.callBackOnEveryValid === 'boolean' ? options.callBackOnEveryValid : true;
            if (options.chainOrder && options.chainOrder.length > 0) {
                this.options.chainedGroups.groups = options.chainOrder;
                this.useRanking = true;
            }
            if (!this.useRanking && options.watchBroken) {
                console.error("Vasilisk: cannot apply chain watching without a chain");
            }
            else if (this.useRanking && options.watchBroken) {
                this.options.chainedGroups.watchChainBroken = options.watchBroken;
            }
            if (!this.useRanking && options.brokenCallback) {
                console.error("Vasilisk: cannot assign chain broken watch callback without a chain");
            }
            else if (this.useRanking && options.brokenCallback) {
                this.options.chainedGroups.brokenChainCallback = options.brokenCallback;
            }
        }
    }
    /**
     * CreateGroup - create a logical group of elements
     *
     * @param gid string - group id
     * @param onValid function - callback triggered when group is valid
     * @param onInvalid function - callback triggered when group is invalid
     * @param elems array - input fields to be watched
     *
     * @returns void
     */
    Vasilisk.prototype.CreateGroup = function (gid, onValid, onInvalid, elems) {
        var _this = this;
        this.GroupExists(gid).then(function (exists) {
            if (!exists) {
                var e = elems.map(function (value) {
                    var dome = _this.GetDOMElement(value.id);
                    var pristine = value.pristine ? value.pristine : false;
                    var fcb = value.failed ? value.failed : _this.DevNull;
                    return { e: { internal: dome, isValid: pristine, failed: fcb }, validation: value.validation };
                });
                _this.groups.push({ gid: gid, elems: e, onValid: onValid, onInvalid: onInvalid, isValid: false, isDirty: true });
                _this.BindEventListener(_this.groups[_this.groups.length - 1]);
            }
            else {
                console.error("Group already exists " + exists.gid);
            }
        }).catch(function (e) {
            console.error(e);
        });
    };
    /**
     * DevNull - > /dev/null :)
     */
    Vasilisk.prototype.DevNull = function () { };
    /**
     * IsCurrentlyValid - poll if the specified group is in a valid state.
     *
     * @param gid string - group id
     *
     * @returns Promise<boolean>
     */
    Vasilisk.prototype.IsCurrentlyValid = function (gid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.GroupExists(gid).then(function (group) {
                if (group) {
                    resolve(group.isValid);
                }
                else {
                    reject('group does not exist');
                }
            }).catch(function (e) {
                console.error(e);
            });
        });
    };
    /**
     * ForceValidationQuiet - Re-evaluate group validity
     *
     * @param gid string - Group id
     * @param isDirty boolean - mark group as Dirty
     */
    Vasilisk.prototype.ForceValidationQuiet = function (gid, isDirty) {
        if (isDirty === void 0) { isDirty = true; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, new Promise(function (resolve, reject) {
                        _this.GetGroupById(gid).then(function (g) {
                            if (g !== undefined) {
                                var i = g.elems.length;
                                var overallValid_1 = 0;
                                g.elems.forEach(function (e) {
                                    if (e.validation(e.e.internal) === true) {
                                        e.e.isValid = true;
                                        overallValid_1 += 1;
                                        g.isDirty = isDirty;
                                    }
                                    else {
                                        e.e.isValid = false;
                                    }
                                });
                                if (overallValid_1 === g.elems.length) {
                                    g.isValid = true;
                                }
                                resolve(g.isValid);
                            }
                            else {
                                console.error("Couldn't find a group");
                                resolve(false);
                            }
                        }).catch(function (e) {
                            console.error(e);
                            reject();
                        });
                    })];
            });
        });
    };
    /**
    * ForceValidation - immitate change event and call specified callbacks.
    * ! Marks a group as dirty
    *
    * @param gid string - Group id
    * * @param isDirty boolean - mark group as Dirty
    */
    Vasilisk.prototype.ForceValidation = function (gid, isDirty) {
        var _this = this;
        if (isDirty === void 0) { isDirty = true; }
        this.GetGroupById(gid).then(function (g) {
            if (g !== undefined) {
                var i = g.elems.length;
                g.elems.forEach(function (e) {
                    if (e.validation(e.e.internal) === true) {
                        e.e.isValid = true;
                        g.isDirty = isDirty;
                    }
                    else {
                        e.e.isValid = false;
                        e.e.failed(e.e.internal);
                    }
                    if (_this.options.callBackOnEveryValid) {
                        _this.WatchDirty();
                    }
                    else {
                        _this.Watch();
                    }
                });
            }
            else {
                console.error("Couldn't find a group");
            }
        }).catch(function (e) {
            console.error(e);
        });
    };
    /**
     * ForceValidationQuietChained - Reevaluate all Vasilisk groups and mark top group as dirty
     *
     * @param ignoreGroupsIds Array<string> - groups to ignore in validation
     * @default []
     *
     * @returns Promise string | null (if non of groups is valid)
     */
    Vasilisk.prototype.ForceValidationQuietChained = function (ignoreGroupsIds) {
        var _this = this;
        if (ignoreGroupsIds === void 0) { ignoreGroupsIds = []; }
        if (!this.options.chainedGroups.groups || this.options.chainedGroups.groups.length === 0) {
            console.error("Cannot use ForceValidationQuietChained function on a Vasilisk without a chain");
            return Promise.resolve(null);
        }
        return new Promise(function (resolve, reject) {
            _this.groups.forEach(function (g) { return __awaiter(_this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!(ignoreGroupsIds.length !== 0)) return [3 /*break*/, 4];
                            if (!(ignoreGroupsIds.indexOf(g.gid) !== -1)) return [3 /*break*/, 1];
                            return [2 /*return*/];
                        case 1: return [4 /*yield*/, this.ForceValidationQuiet(g.gid, false)];
                        case 2:
                            _a.sent();
                            _a.label = 3;
                        case 3: return [3 /*break*/, 6];
                        case 4: return [4 /*yield*/, this.ForceValidationQuiet(g.gid, false)];
                        case 5:
                            _a.sent();
                            _a.label = 6;
                        case 6: return [2 /*return*/];
                    }
                });
            }); });
            _this.GetTopValidGroup(_this.groups[0].gid).then(function (group) {
                if (group) {
                    _this.IsCurrentlyValid(group.gid).then(function (v) {
                        if (v) {
                            group.isDirty = true;
                            resolve(group.gid);
                        }
                        else {
                            resolve(null);
                        }
                    });
                }
                else {
                    resolve(null);
                }
            });
        });
    };
    /**
     * BindEventListener - apply event listeners on elements in the group
     *
     * @returns void
     */
    Vasilisk.prototype.BindEventListener = function (g) {
        var _this = this;
        var i = g.elems.length;
        g.elems.forEach(function (e) {
            e.e.internal.addEventListener('change', function (event) {
                if (e.validation(e.e.internal) === true) {
                    e.e.isValid = true;
                    g.isDirty = true;
                }
                else {
                    e.e.isValid = false;
                    e.e.failed(e.e.internal);
                }
                if (_this.options.callBackOnEveryValid) {
                    _this.WatchDirty();
                }
                else {
                    _this.Watch();
                }
            });
        });
    };
    /**
     * Watch - polling group validity and invoking corresponding callbacks
     *
     * @returns void
     */
    Vasilisk.prototype.WatchDirty = function () {
        var _this = this;
        this.groups.forEach(function (g) {
            var numOfValidFields = 0;
            g.elems.forEach(function (e) {
                if (e.e.isValid) {
                    numOfValidFields++;
                }
            });
            if (numOfValidFields === g.elems.length && g.isDirty) {
                g.isValid = true;
                if (_this.useRanking) {
                    _this.GetTopValidGroup(g.gid).then(function (top) {
                        if (top) {
                            if (top.isValid) {
                                top.onValid(top.gid);
                            }
                            else {
                                if (_this.options.chainedGroups.watchChainBroken) {
                                    _this.options.chainedGroups.brokenChainCallback(_this.GetAllInvalidFields(top.gid));
                                }
                                else {
                                    console.error('Vasilisk: Broken chain occured at: ' + top.gid);
                                }
                            }
                        }
                        else {
                            console.error('Vasilisk: Broken chain occured');
                        }
                    }).catch(function (e) {
                        console.error(e);
                    });
                }
                else {
                    g.onValid(g.gid);
                }
                g.isDirty = false;
            }
            else if (numOfValidFields !== g.elems.length) {
                if (g.isValid) {
                    g.isValid = !g.isValid;
                    g.onInvalid(g.gid, g.elems.filter(function (val, index, array) {
                        return val.e.isValid === false;
                    }));
                }
            }
        });
    };
    /**
     * WatchDirty - polling group validity + change if occured in already valid fields and invoking corresponding callbacks
     *
     * @returns void
     */
    Vasilisk.prototype.Watch = function () {
        var _this = this;
        this.groups.forEach(function (g) {
            var numOfValidFields = 0;
            g.elems.forEach(function (e) {
                if (e.e.isValid) {
                    numOfValidFields++;
                }
            });
            if (numOfValidFields === g.elems.length) {
                if (!g.isValid) {
                    g.isValid = !g.isValid;
                    if (_this.useRanking) {
                        _this.GetTopValidGroup(g.gid).then(function (top) {
                            if (top) {
                                if (top.isValid) {
                                    top.onValid(top.gid);
                                }
                                else {
                                    if (_this.options.chainedGroups.watchChainBroken) {
                                        _this.options.chainedGroups.brokenChainCallback(_this.GetAllInvalidFields(top.gid));
                                    }
                                    else {
                                        console.error('Vasilisk: Broken chain occured at: ' + top.gid);
                                    }
                                }
                            }
                            else {
                                console.error('Vasilisk: Broken chain occured');
                            }
                        }).catch(function (e) {
                            console.error(e);
                        });
                    }
                    else {
                        g.onValid(g.gid);
                    }
                }
            }
            else {
                if (g.isValid) {
                    g.isValid = !g.isValid;
                    g.onInvalid(g.gid, g.elems.filter(function (val, index, array) {
                        return val.e.isValid === false;
                    }));
                }
            }
        });
    };
    /**
     * GetAllInvalidFields - get all invalid predecessors' inputs of the current group
     * @description Only if group ranking is assigned
     * @param gid string - group id
     *
     * @returns Array<{gid: string, elem: HTMLInputElement}>
     */
    Vasilisk.prototype.GetAllInvalidFields = function (gid) {
        return __awaiter(this, void 0, void 0, function () {
            var curRank, failedItems, _loop_1, this_1, back;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        curRank = this.GetGroupRankingIndex(gid);
                        failedItems = new Array();
                        _loop_1 = function (back) {
                            var g;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this_1.GetGroupById(this_1.options.chainedGroups.groups[back])];
                                    case 1:
                                        g = _a.sent();
                                        if (g && !g.isValid) {
                                            g.elems.forEach(function (e) {
                                                if (!e.e.isValid) {
                                                    if (g) { // suppress LINTER warning only
                                                        failedItems.push({ gid: g.gid, elem: e.e.internal });
                                                    }
                                                }
                                            });
                                        }
                                        return [2 /*return*/];
                                }
                            });
                        };
                        this_1 = this;
                        back = curRank;
                        _a.label = 1;
                    case 1:
                        if (!(back >= 0)) return [3 /*break*/, 4];
                        return [5 /*yield**/, _loop_1(back)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        back--;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/, failedItems];
                }
            });
        });
    };
    /**
     * GetTopValidGroup - Get the top-most valid group
     * @description Only if group ranking is assigned
     *
     * @param gid string - group id
     *
     * @returns Promise - group if found, undefined otherwise
     */
    Vasilisk.prototype.GetTopValidGroup = function (gid) {
        return __awaiter(this, void 0, void 0, function () {
            var curRank, maxIdx, back, g, g, next;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        curRank = this.GetGroupRankingIndex(gid);
                        maxIdx = this.options.chainedGroups.groups.length;
                        if (!this.options.chainedGroups.watchChainBroken) return [3 /*break*/, 4];
                        back = curRank;
                        _a.label = 1;
                    case 1:
                        if (!(back >= 0)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.GetGroupById(this.options.chainedGroups.groups[back])];
                    case 2:
                        g = _a.sent();
                        if (g && !g.isValid) {
                            return [2 /*return*/, g];
                        }
                        _a.label = 3;
                    case 3:
                        back--;
                        return [3 /*break*/, 1];
                    case 4:
                        if (!(curRank < maxIdx)) return [3 /*break*/, 12];
                        return [4 /*yield*/, this.GetGroupById(this.options.chainedGroups.groups[curRank])];
                    case 5:
                        g = _a.sent();
                        if (!(g && g.isValid)) return [3 /*break*/, 9];
                        if (!(curRank + 1 < maxIdx)) return [3 /*break*/, 7];
                        return [4 /*yield*/, this.GetTopValidGroup(this.options.chainedGroups.groups[curRank + 1])];
                    case 6:
                        next = _a.sent();
                        if (next && next.isValid) {
                            return [2 /*return*/, next];
                        }
                        else {
                            return [2 /*return*/, g];
                        }
                        return [3 /*break*/, 8];
                    case 7: return [2 /*return*/, g];
                    case 8: return [3 /*break*/, 11];
                    case 9: return [4 /*yield*/, this.GetGroupById(this.options.chainedGroups.groups[curRank - 1])];
                    case 10: return [2 /*return*/, _a.sent()];
                    case 11:
                        curRank++;
                        return [3 /*break*/, 4];
                    case 12: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * GetGroupRankingIndex - get current rank from ranking array
     * @description Only if group ranking is assigned
     *
     * @param gid string - group id
     *
     * @returns number - index in ranking
     */
    Vasilisk.prototype.GetGroupRankingIndex = function (gid) {
        return this.options.chainedGroups.groups.indexOf(gid);
    };
    /**
     * GetGroupById - Get the group instance by its group id
     *
     * @param gid string - group id
     *
     * @returns Promise - group if found, undefined otherwise
     */
    Vasilisk.prototype.GetGroupById = function (gid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.GroupExists(gid).then(function (nextGroup) {
                if (nextGroup) {
                    resolve(nextGroup);
                }
                else {
                    resolve(undefined);
                }
            }).catch(function (e) {
                reject(undefined);
            });
        });
    };
    /**
     * GetDOMElement - Retrieve input on document
     * @param id string - <input id='{{this}}'>
     * @returns HTMLInputElement
     */
    Vasilisk.prototype.GetDOMElement = function (id) {
        var e = document.getElementById(id);
        if (!e) {
            console.error("Couldn\'t find " + id);
        }
        return e;
    };
    /**
     * GroupExists - checks if a group exists in a context
     * @param gid string - group id
     *
     * @returns boolean
     */
    Vasilisk.prototype.GroupExists = function (gid) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.groups.forEach(function (element) {
                if (element.gid === gid) {
                    resolve(element);
                }
            });
            resolve(undefined);
        });
    };
    return Vasilisk;
}());
exports.Vasilisk = Vasilisk;
