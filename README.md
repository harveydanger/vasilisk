# VasiliskJS

Library that allows to combile inputs in logical groups. Library listens the validity of groups and invokes callbacks.
Built on native events, so no polling and busy waiting.

## Installation

```
npm install -g vasilisk
```

## How to use

Object instantiation signature is the following (<> - optional values):

```
variable = new Vasilisk(<{<callBackOnEveryValid : bool>, <<chainOrder : [string]>, <watchBroken : bool>, <brokenCallback : function>>}> )
```

1. Create a new object `var v = new Vasilisk({callBackOnEveryValid: false, chainOrder: ['inp-grp-1', 'inp-grp-2'], watchBroken: true, brokenCallback: (prom)=>{console.log('promise)}});` 
- *callBackOnEveryValid* is **optional** - called every time if value in a group has changed, though it is still valid. By default, *callBackOnEveryValid* is set to true.
- *chainOrder* is **optional** -  array of groups that will be created under Vasilisk. Group order identifies, which callback to invoke. For example, if 3 grops are created, first group is triggered to Valid state, Vasilisk finds closest down the array (0->n) and triggers its callback. See First group in the example.html
- *watchBroken* is **optional** - boolean, defines that Vasilisk should check if previous groups in a chain are also valid before invoking current group callback
- *brokenCallback* is **optional** - function, triggered when the condition in watchBroken routine is triggered.
2. Create input group:
   `Signature: gid: string, onValid: Callback, onInvalid: Callback, elems: [{id: string, validation: Validation, <pristine: boolean>}]`
```
v.CreateGroup('calculator',
            (id) => {
                console.log("Valid: " + id)
            }, (id) => {
                console.log("Invalid: " + id)
            }, [{
                    id: "i-1",
                    validation: (e) => {
                        return +e.value === 5;
                    }
                },
                {
                    id: "i-2",
                    validation: (e) => {
                        return +e.value === 10;
                    },
                    pristine: true,
                    failed: (e) =>{
                        console.log("failed");
                    }
                }
            ]);
```
where: 
- *pristine* defines if the input field already has a value and is suggested to be valid.
- *failed* defines individual input failure callback function.

## Validation

Validation is a function that must return a boolean value. Injected parameter is a DOM HTML input field. e.g.

```
function myValidation(domElement){
    return +domElement.value > 0;
}
```

## Example

**See [example.html](https://github.com/HarveyDanger/VasiliskJS/blob/master/example.html)**

***Copyright 2019 Vladimir Sukhov, GPLv3-higher***